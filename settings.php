<?php

function get_antique_subtitles_default_options() {

    $defaults = array(
        'tag' => 'span',
        'position_rel' => 'after',
        'position_num' => '1',
        'position_tag' => 'h1',
        'font_size' => '18',
        'font_color_custom' => '#000000',
        'font_color_theme' => ''
    );

    return $defaults;
}

function get_antique_subtitles_options() {

    $defaults = get_antique_subtitles_default_options();

    $options = wp_parse_args(
            args: get_option('antique_subtitles_options'),
            defaults: $defaults
    );

    return $options;
}

add_action(
        hook_name: 'admin_menu',
        callback: 'antique_subtitles_options_page'
);

function antique_subtitles_options_page() {

    if (!function_exists('antique_plugins_admin_menu')) {
        add_menu_page(
                page_title: __('Antique Subtitles', 'antique-subtitles'),
                menu_title: __('Antique Subtitles', 'antique-subtitles'),
                capability: 'manage_options',
                menu_slug: 'antique_subtitles',
                callback: 'antique_subtitles_options_page_html'
        );
    } else {
        add_submenu_page(
                parent_slug: 'antique_plugins',
                page_title: __('Antique Plugins', 'antique-subtitles')
                . ' &rsaquo; ' . __('Subtitles', 'antique-subtitles'),
                menu_title: __('Subtitles', 'antique-subtitles'),
                capability: 'manage_options',
                menu_slug: 'antique_subtitles',
                callback: 'antique_subtitles_options_page_html'
        );
    }
}

add_action(
        hook_name: 'admin_init',
        callback: 'antique_subtitles_remove_menu_page'
);

function antique_subtitles_remove_menu_page() {
    if (!function_exists('antique_plugins_admin_menu')) {
        remove_menu_page(menu_slug: 'antique_subtitles');
    }
}

function antique_subtitles_options_page_html() {

    if (!current_user_can(capability: 'manage_options')) {
        return;
    }
    ?>

    <div class="wrap antique-plugin-settings-wrap">
        <h1><?php echo esc_html(get_admin_page_title()); ?></h1>

        <?php settings_errors(setting: 'antique_subtitles_updated_message'); ?>
        <?php settings_errors(setting: 'antique_subtitles_reset_message'); ?>

        <p class="antique-description">
            <?php
            esc_html_e('This plugin allows you to add subtitles to your pages. '
                    . 'When you create or edit pages, there is a "Subtitle" '
                    . 'section in the right settings sidebar. Insert a custom '
                    . 'subtitle into the text field therein and update the '
                    . 'page.',
                    'antique-subtitles');
            ?>
        </p>
        <form action="options.php" method="post">
            <?php
            settings_fields(option_group: 'antique_subtitles_fields');
            do_settings_sections(page: 'antique_subtitles');
            ?>
            <div class="antique-plugin-submit-buttons-wrap">
                <?php
                submit_button(
                        text: __('Save changes', 'antique-subtitles'),
                        type: 'primary',
                        name: 'updated',
                        wrap: false
                );
                submit_button(
                        text: __('Reset', 'antique-subtitles'),
                        type: 'secondary',
                        name: 'reset',
                        wrap: false
                );
                ?>
            </div>
            <?php antique_subtitles_where_is_used(); ?>
        </form>
    </div>

    <?php
}

function antique_subtitles_settings_sanitize_cb($input) {

    if (isset($_POST['updated'])) {

        add_settings_error(
                setting: 'antique_subtitles_updated_message',
                code: 'antique_subtitles_updated_message_code',
                message: __('Your settings have been saved.', 'antique-subtitles'),
                type: 'updated'
        );
    } else if (isset($_POST['reset'])) {

        add_settings_error(
                setting: 'antique_subtitles_reset_message',
                code: 'antique_subtitles_reset_message_code',
                message: __('Your settings have been reset.', 'antique-subtitles'),
                type: 'updated'
        );

        return array();
    }

    return $input;
}

function antique_subtitles_where_is_used() {

    $meta_key = '_antique_subtitle_meta_key';

    $query = new WP_Query(array(
        'post_type' => array('post', 'page'),
        'posts_per_page' => -1
    ));

    $pages = array();

    if ($query->have_posts()) {
        while ($query->have_posts()) {

            $query->the_post();
            $id = get_the_ID();

            $meta_exists = metadata_exists(
                    meta_type: 'post',
                    object_id: $id,
                    meta_key: $meta_key
            );

            if ($meta_exists) {
                $subtitle = get_post_meta(
                        post_id: get_the_ID(),
                        key: $meta_key,
                        single: true
                );
                $is_not_empty = $subtitle != '';
            } else {
                $is_not_empty = false;
            }

            if ($is_not_empty) {

                $post_type_en = get_post_type();
                if ($post_type_en == 'page') {
                    $post_type = esc_html__('page', 'antique-subtitles');
                } else if ($post_type_en == 'post') {
                    $post_type = esc_html__('post', 'antique-subtitles');
                } else {
                    $post_type = $post_type_en;
                }

                $pages[$id] = array(
                    'post_type' => $post_type,
                    'title' => get_the_title(),
                    'loc' => esc_html__('title', 'antique-subtitles'),
                    'link' => get_permalink(),
                );
            }
        }
    }

    ksort($pages);

    wp_reset_postdata();
    ?>

    <div class="antique-plugin-table-wrap">
        <p><?php
            esc_html_e('Pages and posts with a subtitle:',
                    'antique-subtitles');
            ?></p>
        <div id="antique-plugin-toggle-table">
            <span class="show is-displayed"
                  ><?php esc_html_e('Show', 'antique-subtitles'); ?></span>
            <span class="hide"
                  ><?php esc_html_e('Hide', 'antique-subtitles'); ?></span>
        </div>

        <table class="antique-plugin-pages-table">
            <tr>
                <th><?php esc_html_e('ID', 'antique-subtitles'); ?></th>
                <th><?php esc_html_e('type', 'antique-subtitles'); ?></th>
                <th><?php esc_html_e('title', 'antique-subtitles'); ?></th>
                <th><?php esc_html_e('location', 'antique-subtitles'); ?></th>
                <th><?php esc_html_e('link', 'antique-subtitles'); ?></th>
            </tr>
            <?php
            foreach ($pages as $ID => $page) {
                ?>
                <tr>
                    <td><?php esc_html_e($ID); ?></td>
                    <td><?php echo $page['post_type']; ?></td>
                    <td><?php echo $page['title']; ?></td>
                    <td><?php echo $page['loc']; ?></td>
                    <td><a href="<?php echo $page['link']; ?>"
                           target="_blank"><?php
                               esc_html_e('link', 'antique-subtitles');
                               ?></a></td>
                </tr>
                <?php
            }
            ?>
        </table>
    </div>

    <?php
}

add_action(
        hook_name: 'admin_init',
        callback: 'antique_subtitles_settings_init'
);

function antique_subtitles_settings_init() {

    $option_name = 'antique_subtitles_options';

    register_setting(
            option_group: 'antique_subtitles_fields',
            option_name: $option_name,
            args: array(
                'sanitize_callback' => 'antique_subtitles_settings_sanitize_cb'
            )
    );

    add_settings_section(
            id: 'antique_subtitles_settings_section',
            title: __('Settings', 'antique-subtitles'),
            callback: 'antique_subtitles_settings_section_cb',
            page: 'antique_subtitles'
    );

    add_settings_field(
            id: 'tag',
            title: __('Tag:', 'antique-subtitles'),
            callback: 'antique_subtitles_field_tag_cb',
            page: 'antique_subtitles',
            section: 'antique_subtitles_settings_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'tag',
            )
    );

    add_settings_field(
            id: 'position',
            title: __('Position:', 'antique-subtitles'),
            callback: 'antique_subtitles_field_position_cb',
            page: 'antique_subtitles',
            section: 'antique_subtitles_settings_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'position',
            )
    );

    add_settings_field(
            id: 'font_size',
            title: __('Font size:', 'antique-subtitles'),
            callback: 'antique_subtitles_field_font_size_cb',
            page: 'antique_subtitles',
            section: 'antique_subtitles_settings_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'font_size',
            )
    );

    add_settings_field(
            id: 'font_color',
            title: __('Font color:', 'antique-subtitles'),
            callback: 'antique_subtitles_field_font_color_cb',
            page: 'antique_subtitles',
            section: 'antique_subtitles_settings_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'font_color',
            )
    );
}

function antique_subtitles_settings_section_cb($args) {

}

function antique_subtitles_field_tag_cb($args) {

    $field_id = esc_attr($args['label_for']);

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_subtitles_options();
    ?>

    <select id="<?php echo $field_id; ?>"
            name="<?php echo $option_name; ?>[<?php echo $field_id; ?>]"
            autocomplete="off"
            >
        <option value="span" <?php echo isset($options[$field_id]) ? ( selected($options[$field_id], 'span', false) ) : ( '' ); ?>
                >span</option>
        <option value="p" <?php echo isset($options[$field_id]) ? ( selected($options[$field_id], 'p', false) ) : ( '' ); ?>
                >p</option>
        <option value="h2" <?php echo isset($options[$field_id]) ? ( selected($options[$field_id], 'h2', false) ) : ( '' ); ?>
                >h2</option>
        <option value="h3" <?php echo isset($options[$field_id]) ? ( selected($options[$field_id], 'h3', false) ) : ( '' ); ?>
                >h3</option>
    </select>

    <p><?php
        esc_html_e('Specify the HTML tag that contains the subtitle.',
                'antique-subtitles');
        ?></p>

    <?php
}

function antique_subtitles_field_position_cb($args) {

    $field_id = esc_attr($args['label_for']);
    $str_position_rel = esc_attr($field_id . '_rel');
    $str_position_num = esc_attr($field_id . '_num');
    $str_position_tag = esc_attr($field_id . '_tag');

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_subtitles_options();
    ?>

    <div style="
         display: flex;
         flex-direction: row;
         align-items: center;
         ">

        <select id="<?php echo $str_position_rel; ?>"
                name="<?php echo $option_name; ?>[<?php echo $str_position_rel; ?>]"
                style="margin-right: 4px;"
                autocomplete="off"
                >
            <option value="before" <?php echo isset($options[$str_position_rel]) ? ( selected($options[$str_position_rel], 'before', false) ) : ( '' ); ?>>
                <?php esc_html_e('before', 'antique-subtitles'); ?>
            </option>
            <option value="after" <?php echo isset($options[$str_position_rel]) ? ( selected($options[$str_position_rel], 'after', false) ) : ( '' ); ?>>
                <?php esc_html_e('after', 'antique-subtitles'); ?>
            </option>
        </select>

        <input type="number"
               id="<?php echo $str_position_num; ?>"
               name="<?php echo $option_name; ?>[<?php echo $str_position_num; ?>]"
               value="<?php echo is_numeric($options[$str_position_num]) ? esc_attr($options[$str_position_num]) : 1; ?>"
               min="1" max="5"
               style="width: 50px"
               autocomplete="off"
               >
        <label for="<?php echo $str_position_num; ?>"
               style="margin-right: 4px;"
               >
            .
        </label>

        <select id="<?php echo $str_position_tag; ?>"
                name="<?php echo $option_name; ?>[<?php echo $str_position_tag; ?>]"
                autocomplete="off"
                >
            <option value="h1" <?php echo isset($options[$str_position_tag]) ? ( selected($options[$str_position_tag], 'h1', false) ) : ( '' ); ?>
                    >h1</option>
            <option value="h2" <?php echo isset($options[$str_position_tag]) ? ( selected($options[$str_position_tag], 'h2', false) ) : ( '' ); ?>
                    >h2</option>
            <option value="p" <?php echo isset($options[$str_position_tag]) ? ( selected($options[$str_position_tag], 'p', false) ) : ( '' ); ?>
                    >p</option>
        </select>

    </div>

    <p><?php
        esc_html_e('Specify the position where the subtitle is displayed.',
                'antique-subtitles');
        ?></p>

    <?php
}

function antique_subtitles_field_font_size_cb($args) {

    $field_id = esc_attr($args['label_for']);

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_subtitles_options();
    ?>

    <input type="number"
           id="<?php echo $field_id; ?>"
           name="<?php echo $option_name; ?>[<?php echo $field_id; ?>]"
           value="<?php echo is_numeric($options[$field_id]) ? esc_attr($options[$field_id]) : 18; ?>"
           min="1" max="100"
           style="width: 60px"
           autocomplete="off"
           >
    <label for="<?php echo $field_id; ?>">px</label>

    <?php
}

function antique_subtitles_field_font_color_cb($args) {

    $field_id = esc_attr($args['label_for']);
    $str_font_color_custom = esc_attr($field_id . '_custom');
    $str_font_color_theme = esc_attr($field_id . '_theme');

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_subtitles_options();
    ?>

    <div>
        <input type="text"
               class="color-field"
               id="<?php echo $str_font_color_custom; ?>"
               name="<?php echo $option_name; ?>[<?php echo $str_font_color_custom; ?>]"
               value="<?php echo esc_attr($options[$str_font_color_custom]); ?>"
               data-default-color="#000000"
               autocomplete="off"
               >
    </div>
    <div style="
         margin-top: 14px;
         display: flex;
         flex-direction: row;">
        <div>
            <input type="checkbox"
                   id="<?php echo $str_font_color_theme; ?>"
                   name="<?php echo $option_name; ?>[<?php echo $str_font_color_theme; ?>]"
                   <?php echo $options[$str_font_color_theme] != '' ? 'checked="checked"' : ''; ?>
                   autocomplete="off"
                   >
        </div>
        <div style="margin-left: 6px;">
            <label for="<?php echo $str_font_color_theme; ?>">
                <?php
                esc_html_e('Use the font color of the title. This option only '
                        . 'applies if you are using the Antique theme. Upon '
                        . 'activation of another theme the color specified '
                        . 'above is used.',
                        'antique-subtitles');
                ?>
            </label>
        </div>
    </div>

    <?php
}
