��            )         �     �     �     �     �  
   �                  	   1     ;     A     N     W  0   \  5   �     �  	   �     �  �   �  �   �     Z     y     �     �     �     �     �     �     �     �    �  +   �     �               (  
   8     C      F  	   g     q          �     �  '   �  ;   �  
   	  
   	     &	    +	  �   :
  *     '   B     j     o     s     x          �     �     �                                                                                                                   
                 	    Add a subtitle to your pages. Antique Plugins Antique Subtitles Font color: Font size: Hide ID Pages and posts with a subtitle: Position: Reset Save changes Settings Show Specify the HTML tag that contains the subtitle. Specify the position where the subtitle is displayed. Subtitle Subtitles Tag: This plugin allows you to add subtitles to your pages. When you create or edit pages, there is a "Subtitle" section in the right settings sidebar. Insert a custom subtitle into the text field therein and update the page. Use the font color of the title. This option only applies if you are using the Antique theme. Upon activation of another theme the color specified above is used. Your settings have been reset. Your settings have been saved. after before link location page post title type Project-Id-Version: Antique Subtitles
PO-Revision-Date: 2023-05-14 22:14+0200
Last-Translator: 
Language-Team: Simon Garbin
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;esc_html_e;esc_html__;esc_attr__;esc_attr_e
X-Poedit-SearchPath-0: antique-subtitles.php
X-Poedit-SearchPath-1: settings.php
X-Poedit-SearchPath-2: shared/menu.php
 Füge deinen Seiten einen Untertitel hinzu. Antique Plugins Antique Untertitel Schriftfarbe: Schriftgröße: Verstecken ID Seiten und Posts mit Untertitel: Position: Zurücksetzen Änderungen speichern Einstellungen Anzeigen Lege den HTML-Tag des Untertitels fest. Lege die Stelle fest, an der der Untertitel angezeigt wird. Untertitel Untertitel Tag: Mit diesem Plugin kannst du auf deinen Seiten einen Untertitel einblenden. Wenn du eine Seite erstellst oder bearbeitest, gibt es auf der rechten Seitenleiste den Abschnitt "Untertitel". Gib im darin befindlichen Textfeld einen Untertitel ein und aktualisiere die Seite. Verwende die Schriftfarbe des Titels. Diese Einstellung kann nur angewandt werden, wenn das Antique-Theme installiert und aktiviert ist. Sobald ein anderes Theme aktiviert wird, wird die oben festgelegte Farbe verwendet. Deine Einstellungen wurden zurückgesetzt. Deine Einstellungen wurden gespeichert. nach vor Link Stelle Seite Post Titel Typ 