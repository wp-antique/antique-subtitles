(function () {

    const options = antique_subtitles_options;
    const [text, tag] = [...options['subtitle']];
    const [pos_rel, pos_num, pos_tag] = [...options['position']];
    const classes = options['classes'];

    var subtitle = document.createElement(tag);
    subtitle.innerHTML = text;
    subtitle.setAttribute('id', 'subtitle');
    subtitle.setAttribute('class', classes);

    var nodes = document.getElementsByTagName(pos_tag);
    if (nodes.length === 0 || pos_num >= nodes.length) {
        return;
    }

    var sibling = nodes[pos_num];
    if (sibling === null) {
        return;
    }

    var parent = sibling.parentNode;
    if (parent === null) {
        return;
    }

    if (pos_rel === 'before') {
        parent.insertBefore(subtitle, sibling);
    } else {
        parent.insertBefore(subtitle, sibling.nextSibling);
    }

}
)($);