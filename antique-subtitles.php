<?php
/**
 * Plugin Name: Antique Subtitles
 * Plugin URI: https://gitlab.com/wp-antique/plugins/antique-subtitles
 * Description: Add a subtitle to your pages.
 * Version: 1.0.0
 * Author: Simon Garbin
 * Author URI: simongarbin.com
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html.en
 * Text Domain: antique-subtitles
 * Domain Path: /languages
 */
/*
  Copyright 2023 Simon Garbin
  Antique Subtitles is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  any later version.

  Antique Subtitles is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Antique Subtitles. If not, see
  https://www.gnu.org/licenses/gpl-3.0.html.en.
 */
/*
  Comments:
  - Every function is prefixed by the plugin name in order to prevent
  name collisions.
  - In almost all functions named arguments are used in order to facilitate
  the understanding of the code.
  - Single quotation marks are used for php/js code, double quotation marks
  for HTML.
 */

include plugin_dir_path(__FILE__) . '/shared/menu.php';
include plugin_dir_path(__FILE__) . '/shared/register.php';
include plugin_dir_path(__FILE__) . '/settings.php';

defined('ABSPATH') || exit;

/*
  ----------------------------------------
  Translation
  ----------------------------------------
 */

add_action(
        hook_name: 'init',
        callback: 'antique_subtitles_load_textdomain'
);

function antique_subtitles_load_textdomain() {
    load_plugin_textdomain(
            domain: 'antique-subtitles',
            deprecated: false,
            plugin_rel_path: dirname(plugin_basename(__FILE__)) . '/languages'
    );
}

function antique_subtitles_translation_dummy() {
    $plugin_description = __('Add a subtitle to your pages.', 'antique_toc');
}

/*
  ----------------------------------------
  Admin Settings
  ----------------------------------------
 */

add_filter(
        hook_name: 'plugin_action_links_' . plugin_basename(__FILE__),
        callback: 'antique_subtitles_settings_link'
);

function antique_subtitles_settings_link(array $links) {

    $url = admin_url(path: 'admin.php?page=antique_subtitles');
    $link = '<a href="' . esc_html($url) . '">'
            . __('Settings', 'antique-subtitles')
            . '</a>';
    $links[] = $link;

    return $links;
}

/*
  ----------------------------------------
  Meta Box
  ----------------------------------------
 */

function antique_subtitles_get_post_meta($id) {

    $meta_key = antique_subtitles_get_meta_strings()['key'];
    $post_meta = get_post_meta(
            post_id: $id,
            key: $meta_key,
            single: true
    ) ?: '';

    return $post_meta;
}

function antique_subtitles_get_meta_strings() {

    $meta_strings = array(
        'field' => 'antique_subtitles_meta_field',
        'key' => '_antique_subtitles_meta_key',
        'nonce_name' => 'antique_subtitles_meta_nonce',
        'nonce_action' => 'add_antique_subtitles_meta_nonce',
    );

    return $meta_strings;
}

add_action(
        hook_name: 'add_meta_boxes',
        callback: 'antique_subtitles_meta_box'
);

function antique_subtitles_meta_box() {

    add_meta_box(
            id: 'antique_subtitle',
            title: __('Subtitle', 'antique-subtitles'),
            callback: 'antique_subtitles_meta_box_html',
            screen: array(
                'page',
                'post'
            ),
            context: 'side',
            priority: 'high'
    );
}

function antique_subtitles_meta_box_html($post) {

    $meta = antique_subtitles_get_meta_strings();

    $meta_field = $meta['field'];
    $nonce_name = $meta['nonce_name'];
    $nonce_action = $meta['nonce_action'];

    wp_nonce_field(
            action: $nonce_action,
            name: $nonce_name
    );

    $post_meta = antique_subtitles_get_post_meta(
            id: $post->ID
    );
    ?>

    <div class="components-panel__row">
        <textarea id="<?php esc_attr_e($meta_field); ?>"
                  name="<?php esc_attr_e($meta_field); ?>"
                  style="width: 100%; margin: 0;"
                  rows=1
                  autocomplete="off"
                  ><?php echo $post_meta; ?></textarea>
    </div>

    <?php
}

add_action(
        hook_name: 'save_post',
        callback: 'antique_subtitles_save_meta'
);

function antique_subtitles_save_meta($post_id) {

    $meta = antique_subtitles_get_meta_strings();

    $meta_field = $meta['field'];
    $meta_key = $meta['key'];

    $nonce_name = $meta['nonce_name'];
    $nonce_action = $meta['nonce_action'];

    if (!isset($_POST[$nonce_name])) {
        return;
    }

    if (!wp_verify_nonce(
                    nonce: $_POST[$nonce_name],
                    action: $nonce_action)
    ) {
        return;
    }

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

    if (
            isset($_POST['post_type']) && (
            'page' == $_POST['post_type'] || 'post' == $_POST['post_type']
            )
    ) {
        if (!current_user_can('edit_page', $post_id)) {
            return;
        }
    }

    if (!isset($_POST[$meta_field])) {
        return;
    }


    if (array_key_exists('antique_subtitle_meta_field', $_POST)) {
        $subtitle = wp_kses(
                content: $_POST['antique_subtitle_meta_field'],
                allowed_html: $allowed_html
        );
    } else {
        $subtitle = '';
    }

    $sanitized = antique_subtitles_sanitize_meta(
            post_meta: $_POST[$meta_field]
    );

    update_post_meta(
            post_id: $post_id,
            meta_key: $meta_key,
            meta_value: $sanitized
    );
}

function antique_subtitles_sanitize_meta($post_meta) {

    $allowed_html = array(
        'a' => array(
            'href' => array(),
            'target' => array()
        ),
        'b' => array(),
        'strong' => array(),
        'em' => array(),
        'br' => array()
    );

    $sanitized = wp_kses(
            content: $post_meta,
            allowed_html: $allowed_html
    );

    return $sanitized;
}


/*
  ----------------------------------------
  Styles and Scripts
  ----------------------------------------
 */

add_action(
        hook_name: 'wp_head',
        callback: 'antique_subtitles_custom_style'
);

function antique_subtitles_custom_style() {

    $options = get_antique_subtitles_options();
    $is_antique = $options['font_color_theme'] == 'on' && wp_get_theme() == 'Antique';

    if ($is_antique) {
        return;
    }

    $style = sprintf(
            '<style id="antique-subtitles-custom-css">'
            . '.entry-subtitle {'
            . 'font-size: %spx;'
            . 'color: %s;'
            . '}'
            . '</style>',
            $options['font_size'],
            $options['font_color_custom']
    );

    echo $style;
}

add_action(
        hook_name: 'wp_enqueue_scripts',
        callback: 'antique_subtitles_insert_script',
        priority: 999
);

function antique_subtitles_insert_script() {

    if (!is_singular()) {
        return;
    }

    $options = get_antique_subtitles_options();
    $is_antique = $options['font_color_theme'];

    // 'post-subtitle' always remains subtitle class
    $classes = $is_antique ? 'page-subtitle post-subtitle' :
            'entry-subtitle post-subtitle';

    $text = antique_subtitles_get_post_meta(
            id: get_the_ID()
    );
    if ($text == '') {
        return;
    }
    $tag = $options['tag'];

    $pos_rel = $options['position_rel'];
    $pos_num_as_str = $options['position_num'];
    $pos_num = is_numeric($pos_num_as_str) ? (int) $pos_num_as_str - 1 : 0;
    $pos_tag = $options['position_tag'];

    $options_for_js = array(
        'classes' => $classes,
        'subtitle' => array($text, $tag),
        'position' => array($pos_rel, $pos_num, $pos_tag),
    );

    $handle = 'antique-subtitle-insert';

    wp_register_script(
            handle: $handle,
            src: plugins_url('/assets/js/insert.js', __FILE__),
            deps: array(),
            ver: false,
            in_footer: true
    );

    wp_localize_script(
            handle: $handle,
            object_name: 'antique_subtitles_options',
            l10n: $options_for_js
    );

    wp_enqueue_script(handle: $handle);
}
