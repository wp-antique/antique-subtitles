<?php

/*
  ----------------------------------------
  Plugins
  ----------------------------------------
 */

if (!function_exists('antique_plugins_is_active')) {

    function antique_plugins_is_active($plugin) {

        if (in_array(
                        $plugin . '/' . $plugin . '.php',
                        apply_filters(
                                hook_name: 'active_plugins',
                                value: get_option('active_plugins')
                        )
                )) {
            return True;
        } else {
            return False;
        }
    }

}

if (!function_exists('antique_plugins_get_info')) {

    function antique_plugins_get_info() {

        $plugins = array(
            'antique-section-search' => array(
                'domain' => 'section-search',
                'slug' => 'section_search',
                'heading' => esc_html__('Section search', 'antique'),
                'is-collapsible' => true,
                'has-sidebar-meta' => true,
                'is-active' => antique_plugins_is_active(
                        plugin: 'antique-section-search'
                ),
            ),
            'antique-info-box' => array(
                'domain' => 'info-box',
                'slug' => 'info_box',
                'heading' => esc_html__('Info box', 'antique'),
                'is-collapsible' => true,
                'has-sidebar-meta' => true,
                'is-active' => antique_plugins_is_active(
                        plugin: 'antique-info-box'
                ),
            ),
            'antique-toc' => array(
                'domain' => 'toc',
                'slug' => 'toc',
                'heading' => esc_html__('Table of contents', 'antique'),
                'is-collapsible' => true,
                'has-sidebar-meta' => true,
                'is-active' => antique_plugins_is_active(
                        plugin: 'antique-toc'
                ),
            ),
            'antique-sibling-pages' => array(
                'domain' => 'sibling-pages',
                'slug' => 'sibling_pages',
                'heading' => esc_html__('Sibling pages', 'antique'),
                'is-collapsible' => true,
                'has-sidebar-meta' => true,
                'is-active' => antique_plugins_is_active(
                        plugin: 'antique-sibling-pages'
                ),
            ),
            'antique-link-boxes' => array(
                'domain' => 'link-boxes',
                'slug' => 'link_boxes',
                'heading' => esc_html__('Link boxes', 'antique'),
                'is-collapsible' => false,
                'has-sidebar-meta' => false,
                'is-active' => antique_plugins_is_active(
                        plugin: 'antique-link-boxes'
                ),
            ),
            'antique-reading-time' => array(
                'domain' => 'reading-time',
                'slug' => 'reading_time',
                'heading' => esc_html__('Reading time', 'antique'),
                'is-collapsible' => false,
                'has-sidebar-meta' => false,
                'is-active' => antique_plugins_is_active(
                        plugin: 'antique-reading-time'
                ),
            ),
            'antique-subtitles' => array(
                'domain' => 'subtitles',
                'slug' => 'subtitles',
                'heading' => esc_html__('Subtitle', 'antique'),
                'is-collapsible' => false,
                'has-sidebar-meta' => false,
                'is-active' => antique_plugins_is_active(
                        plugin: 'antique-subtitles'
                ),
            ),
        );

        return $plugins;
    }

}

/*
  ----------------------------------------
  Admin style
  ----------------------------------------
 */

if (!function_exists('antique_plugins_admin_style')) {

    add_action(
            hook_name: 'admin_enqueue_scripts',
            callback: 'antique_plugins_admin_style'
    );

    function antique_plugins_admin_style() {

        $handle = 'antique-plugins-admin';

        wp_register_style(
                handle: $handle,
                src: plugins_url('/shared/css/admin.css', dirname(__FILE__)),
                deps: array(),
                ver: false,
                media: 'all'
        );

        wp_enqueue_style(handle: $handle);
    }

}

/*
  ----------------------------------------
  Admin script
  ----------------------------------------
 */

if (!function_exists('antique_plugins_admin_script')) {

    add_action(
            hook_name: 'admin_enqueue_scripts',
            callback: 'antique_plugins_admin_script'
    );

    function antique_plugins_admin_script() {

        $handle = 'antique-plugins-admin';

        wp_register_script(
                handle: $handle,
                src: plugins_url('/shared/js/admin.js', dirname(__FILE__)),
                deps: array('wp-color-picker'),
                ver: false,
                in_footer: true
        );

        wp_enqueue_script(handle: $handle);

        wp_enqueue_style(handle: 'wp-color-picker');
    }

}